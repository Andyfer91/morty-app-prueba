import {useParams, Link} from 'react-router-dom';
import {useState, useEffect} from 'react';
import axios from 'axios';

import {Spinner} from '../Components/Buscador/Spinner.jsx';
import {ListadoEpisodio} from '../Components/Episodio/ListadoEpisodio.jsx';

export function Personaje(){
  let {personajeId} = useParams();
  let {personajeNombre} = useParams();
  let [personaje, setPersonajes] =useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() =>{
        axios.get(`https://rickandmortyapi.com/api/character/${personajeId}`)
        .then((respuesta) =>{
          let {episode} = respuesta.data;

          //arreglo de promesas, de cada episodio
          let peticionesEpisodios = episode.map((urlEpisodio) => 
          axios.get(urlEpisodio)
          );

          //ejecutamos todas las promesas al tiempo
          Promise.all(peticionesEpisodios).then(respuestaEpisodios =>{
            //guardamos los episodios en el .data
            let datosFormateados = respuestaEpisodios.map((episodio) => 
            episodio.data);
            //guargamos los datos de los episodios
            setEpisodios(datosFormateados);
          })
            setPersonajes(respuesta.data);
          })
  }, [])

  function getEstilosStatus(status) {
    let color="green";
    if (personaje.status === "unknown") {
        color = "gray";
    }
    
    if (personaje.status === 'Dead') {
        color = "red";
    }
    const estilocirculo ={
    width: "18px",
    height: "18px",
    display: "inline-block",
    backgroundColor: color,
    borderRadius: "50%",
    marginRight: "10px"
  };
  return estilocirculo;
}

  return(
  <div>  
    <h1 className = "py-4">{personajeNombre}</h1>
    { personaje ? 
    <div className="container col-5 py-2">
      <div className="card mb-3 text-white" 
      style={{
        maxWidth: "600px",
        background: "black",
        boxShadow: "3px 10px 5px #9E9E9E", 
      }}> {/* Propiedades de la tarjeta principal*/}

        <div className="row g-0">

          <div className="col-md-6">
            {/*col-md afecta la tarjeta interna
            la que muestra la foto personaje */}
            <img 
            src= {personaje.image} 
            className="img-fluid rounded-start"
            style = {{
              height: "100%", 
              objectFit: "cover",
              padding: "3px"}}
            alt={personaje.name}
            /> 
          </div> {/* propiedades de la imagen*/}

            <div className="col-md-6">
            {/*col-md afecta la tarjeta interna
            la que muestra la información del personaje */}
            <div className="card-body">
              <p> 
                  <span style={getEstilosStatus(personaje.status)} // los "..." desempaquetan la constante
                  ></span>{personaje.species} - {personaje.status}
              </p>
              <p className="mb-2 text-muted">last know location</p>
              <p>{personaje.location? personaje.location.name : ''}</p>
              {/** operador de encadenamiento opcional, 
               * permite no hacer validación del encadenamiento de un objeto */}
              <p className="mb-2 text-muted">origin universe</p>
              <p>{personaje.origin? personaje.origin.name : ''}</p>
              {/* Propiedades de la tarjeta secundaria */}
            </div>
            </div>

        </div>
      </div>
    </div>
  : <Spinner/> } 
  <h2 className="py-5">Episodios</h2>
  <div>
    {episodios ?
    <div className ="row px-4">
      {/*{JSON.stringify(episodios)}*/}
      <ListadoEpisodio episodios={episodios}/>
      </div> 
    : <Spinner/>}
  </div>
  <Link  to={`/`}
  style={{textdecoration: 'none', color: 'initial'}}> 
  <div className="py-5"><h3>Back to Home</h3></div>
  </Link>
  </div>
    )
  }