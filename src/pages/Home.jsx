import {Banner} from '../Components/Navbar/Banner.jsx';
import {Buscador} from '../Components/Buscador/Buscador.jsx';
import {Listadopersonajes} from '../Components/Personajes/Listadopersonajes.jsx'; 
import {useState} from 'react'

export function Home(){
  let [buscador, setBuscador] = useState('')
  return(
  <div>
      <Banner/>
      <div className="container-fluid text-start">
        <h2 className ='py-4'>Personajes destacados</h2>

        <Buscador valor={buscador} onBuscar={setBuscador}/>

        <Listadopersonajes buscar={buscador}/>
      </div>
  </div>
  )
}