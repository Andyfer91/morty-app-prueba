import {Link} from "react-router-dom"

export function Personajeitem ({
  id,
  image, 
  name, 
  status, 
  species, 
  origin, 
  location
}) {

  let color = "green";
  if (status === "unknown") {
      color = "gray";
  }
  
  if (status === 'Dead') {
      color = "red";
  }

    const EstadoPersonaje ={
    width: "18px",
    height: "18px",
    display: "inline-block",
    backgroundColor: color,
    borderRadius: "50%",
    marginRight: "10px"
}


  return (
        <div className="container col-4 py-2">
          <Link  to={`/personaje/${name}/${id}`} 
          style={{textdecoration: 'none', color: 'initial'}}>
        <div className="card mb-3 text-white" 
        style={{
          maxWidth: "540px",
          background: "black",
          boxShadow: "3px 10px 5px #9E9E9E",
          
        }}>
  <div className="row g-0">
    <div className="col-md-4">
      <img 
      src= {image} 
      className="img-fluid rounded-start"
      style = {{
        height: "100%", 
        objectFit: "cover",
        padding: "3px"}}
      alt={name}

      />
    </div>
    <div className="col-md-8"> 
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p> 
            <span style={{...EstadoPersonaje}} // los "..." desempaquetan la constante
            ></span>{species} - {status}
        </p>
        <p className="mb-0 text-muted">last know location</p>
        <p>{location? location.name : ''}</p>
        {/** operador de encadenamiento opcional, permite no hacer validación del encadenamiento de un objeto */}
        <p className="mb-0 text-muted">origin universe</p>
        <p>{origin? origin.name : ''}</p>
      </div>
    </div>
  </div>
</div>
</Link>
</div>

    );
}