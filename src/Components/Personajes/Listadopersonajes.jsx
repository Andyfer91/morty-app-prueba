import { Personajeitem } from "./Personajeitem.jsx";

import {Spinner} from "../Buscador/Spinner.jsx"
import { useState, useEffect } from "react";
import axios from "axios";

export function Listadopersonajes ({buscar}) {
    const [personajes,setPersonajes] = useState(null);
    let personajesFiltrados = personajes?.results; 
    

    if (buscar && personajes) {
    personajesFiltrados = personajes.results.filter((personaje) => {
    let nombrePersonajeMinuscula = personaje.name.toLowerCase();
    let buscadorMinuscula = buscar.toLowerCase();
    return nombrePersonajeMinuscula.includes(buscadorMinuscula);
    });
};

    useEffect(() => {
        axios.get
        ('https://rickandmortyapi.com/api/character')
        .then((respuesta) =>{
            setPersonajes(respuesta.data);
        });
    }, []);

    return(
    <div className="row py-4">
{/*         {personajes// operador terciario para darle tiempo a la app que cargue
        
        ? personajes.results.map((personajes) => {
        return (
        <Personajeitem {...personajes}/>
        );
        })
        : <Spinner/>} */}{/*Este bloque genera un warning
        recomienda usar una key con diferente nombre
        investigar mas sobre el tema*/}
        
        {personajesFiltrados
        
        ? personajesFiltrados.map((elemento) => {
            return <Personajeitem 
            key ={elemento.id} 
            {...elemento}/>;
        })
        : <Spinner/>}
    </div>
    );
}