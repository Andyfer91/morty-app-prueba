import BannerImg from '../../Assets/Img/BannerImg.png'

export function Banner(){
    return (
        <div className="card bg-dark text-white">
  <img src={BannerImg} className="card-img" alt="Rick y Morty"/>
</div>
    )
}