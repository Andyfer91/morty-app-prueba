export function Footer(){
    return(
        <div className="navbar-dark bg-dark py-2 pt-3">
            <p className="text-center text-white mb-0">
                Proyecto creado por el CAR IV - 11/02/2022
                </p>
                <p className="text-muted ">Todos los derechos reservados Lsv-Tech.SAS</p>
        </div>
    )
}