import { Personajeitem} from "../Personajes/Personajeitem"

export function EpisodioIitem ({name, air_date, episode}){
    return (
        <div className="container col-3">
        <div className=" card text-white bg-dark mb-3" style={{maxWidth: "18rem"}}>
        <div className="card-header">{name}</div>
          <div className="card-body">
          <h5 className="card-title">{air_date}</h5>
          <p className="card-text">{episode}</p>
          </div>
          </div>
        </div>
    )
}

