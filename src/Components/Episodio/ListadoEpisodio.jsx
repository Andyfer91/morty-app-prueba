import {EpisodioIitem} from './EpisodioItem.jsx'

export function ListadoEpisodio ({episodios}) {
    return (
        <div className="row">
            {episodios.map((episodio) => (
                <EpisodioIitem 
                key={episodio.id} 
                {...episodio}/>
            ))}
        </div>
    )
}