
import 'bootstrap/dist/css/bootstrap.min.css';
import {Header} from './Components/Navbar/Header.jsx';
import {Footer} from './Components/Navbar/Footer.jsx';
import {Routes, Route, Link} from 'react-router-dom';

//Importar paginas

import {Home} from './pages/Home.jsx';
import {Personaje} from './pages/Personaje.jsx';

//validar las rutas con un texto que muestre algo en las rutas




function App() {
  return (
    <div className="App text-center">
      <Header/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path='/personaje/:personajeNombre/:personajeId' element={<Personaje/>} />
      </Routes>
      <Footer/>
    </div>
  )
}

export default App
